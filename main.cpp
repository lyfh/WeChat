/****************************************************************************
/*  文 件 名：main.cpp
/*  作    者：
/*  日    期：2016/05/21  星期六  10:43
/*  版 本 号：V 1.0.0
/*  版权描述：Copyright (c) 2016 WeChat.All rights reserved.
/****************************************************************************/

#include <windows.h>
#include "resource.h"
#include "DuiFrame.h"

//Troy 代码需要包含以下内容
#ifdef _DEBUG
#	ifdef _UNICODE
#		pragma comment(lib,"Duilib_d.lib")
#	else
#		pragma comment(lib,"DuilibA_d.lib")
#	endif
#else
#	ifdef _UNICODE
#		pragma comment(lib,"Duilib.lib")
#	else
#		pragma comment(lib,"DuilibA.lib")
#	endif
#endif


int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	CPaintManagerUI::SetInstance(hInstance);
	CDuiFrame DuiFrame;
	DuiFrame.Create(NULL, TEXT("WeChat"), UI_WNDSTYLE_FRAME, WS_EX_WINDOWEDGE);
	DuiFrame.SetIcon(IDI_ICON2);
	DuiFrame.CenterWindow();
	DuiFrame.ShowModal();
	return 0;
}