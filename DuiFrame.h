/****************************************************************************
/*  文 件 名：DuiFrame.h
/*  作    者：
/*  日    期：2016/05/21  星期六  10:35
/*  版 本 号：V 1.0.0
/*  版权描述：Copyright (c) 2016 WeChat.All rights reserved.
/****************************************************************************/
#pragma once
#include <UIlib.h>
using namespace DuiLib;
class CDuiFrame:public WindowImplBase
{
public:
	CDuiFrame();
	~CDuiFrame();
	virtual LPCTSTR GetWindowClassName() const;
	virtual CDuiString GetSkinFile();
	virtual CDuiString GetSkinFolder();
	void Notify(TNotifyUI& msg);

};

