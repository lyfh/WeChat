#include "DuiFrame.h"


CDuiFrame::CDuiFrame()
{
}


CDuiFrame::~CDuiFrame()
{
}

LPCTSTR CDuiFrame::GetWindowClassName() const
{
	return TEXT("WeChat");
}

CDuiString CDuiFrame::GetSkinFile()
{
	return TEXT("main.xml");
}

CDuiString CDuiFrame::GetSkinFolder()
{
	return TEXT("");
}

void CDuiFrame::Notify(TNotifyUI& msg)
{
	if (msg.sType == DUI_MSGTYPE_CLICK)
	{
		if (msg.pSender->GetName() == TEXT("send"))
		{
			//MessageBox(this->GetHWND(), msg.pSender->GetName(), msg.pSender->GetText(), MB_ICONINFORMATION);
			CVerticalLayoutUI *pVerChatBox = static_cast<CVerticalLayoutUI *>(m_pm.FindControl(TEXT("chatbox")));
			CRichEditUI *pREInputBox = static_cast<CRichEditUI *>(m_pm.FindControl(TEXT("inputbox")));
			CHorizontalLayoutUI *pHorMsgBox = new CHorizontalLayoutUI;
			pHorMsgBox->SetAttribute(TEXT("inset"), TEXT("4,4,4,4"));
			pHorMsgBox->SetAttribute(TEXT("height"), TEXT("100"));
			pHorMsgBox->SetAttribute(TEXT("childpadding"), TEXT("8"));
			CControlUI * pCtrlFace = new CControlUI;
			pCtrlFace->SetAttribute(TEXT("pos"), TEXT("0,0,40,40"));
			pCtrlFace->SetAttribute(TEXT("bkimage"), TEXT("file='Src\\tx_01.png'"));
			CVerticalLayoutUI * pVer1 = new CVerticalLayoutUI;
			CRichEditUI * pREName = new CRichEditUI;
			pREName->SetAttribute(TEXT("readonly"), TEXT("true"));
			pREName->SetAttribute(TEXT("height"), TEXT("16"));
			pREName->SetText(TEXT("192.168.1.2:4455"));
			CVerticalLayoutUI * pVer2 = new CVerticalLayoutUI;
			pVer2->SetAttribute(TEXT("bkimage"), TEXT("file='Src\\other_01.png' corner='12,13,9,9'"));
			pVer2->SetAttribute(TEXT("inset"), TEXT("8,8,4,4"));
			CRichEditUI * pREMsg = new CRichEditUI;
			pREMsg->SetText(pREInputBox->GetText());
			pREMsg->SetAttribute(TEXT("readonly"), TEXT("true"));
			pVer2->Add(pREMsg);						//把消息RichEdit塞到一个垂直布局中
			pVer1->Add(pREName);					//把昵称塞到外层垂直布局中
			pVer1->AddAt(pVer2, 1);					//把包含消息RichEdit的垂直布局塞到外层垂直布局中
			pHorMsgBox->Add(pCtrlFace);				//把头像塞到每一条消息框中，即每条消息的水平布局，下同
			pHorMsgBox->Add(pVer1);
			pVerChatBox->Add(pHorMsgBox);			//每一条消息框要塞到总的消息框，或者说是消息窗口
			pVerChatBox->EndDown();					//滚动条滚到底
		}
	}
	

	CNotifyPump::NotifyPump(msg);
}